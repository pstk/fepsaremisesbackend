FROM mono:5.4.1.6
RUN mkdir /remises_backend
COPY ./remises_backend/bin/Debug/ /remises_backend/
EXPOSE 8888
CMD ["mono", "/remises_backend/remises_backend.exe", "-d"]

