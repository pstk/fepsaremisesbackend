drop database `fepsa_remises`;
create database `fepsa_remises`;

CREATE TABLE `fepsa_remises`.`choferes` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `id_remisera` BIGINT NOT NULL,
  `nombre_apellido` VARCHAR(45) NOT NULL,
  `dni` VARCHAR(45) NOT NULL,
  `vigente` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `fepsa_remises`.`remiseras` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `codigo_activacion` VARCHAR(45) NOT NULL,
  `vigente` INT NOT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `fepsa_remises`.`posiciones` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `id_viaje` VARCHAR(45) NOT NULL,
  `id_interno` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `latitud` DECIMAL(10,5) NOT NULL,
  `longitud` DECIMAL(10,5) NOT NULL,
  `rumbo` DECIMAL(10,5) NOT NULL,
  `accuracy` DECIMAL(10,5) NOT NULL,
  `velocidad` BIGINT NOT NULL,
  `fecha_insercion` DATETIME NOT NULL,
  `id_version_logica` BIGINT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `fepsa_remises`.`parametros_logica` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `on_off_rumbo` INT NOT NULL,
  `on_off_tiempo_velocidad` INT NOT NULL,
  `on_off_distancia_velocidad` INT NOT NULL,
  `velocidad_v1` DECIMAL(10,5) NOT NULL,
  `velocidad_v2` DECIMAL(10,5) NOT NULL,
  `velocidad_v3` DECIMAL(10,5) NOT NULL,
  `velocidad_v4` DECIMAL(10,5) NOT NULL,
  `tiempo_t1` DECIMAL(10,5) NOT NULL,
  `tiempo_t2` DECIMAL(10,5) NOT NULL,
  `tiempo_t3` DECIMAL(10,5) NOT NULL,
  `distancia_x1` DECIMAL(10,5) NOT NULL,
  `distancia_x2` DECIMAL(10,5) NOT NULL,
  `distancia_x3` DECIMAL(10,5) NOT NULL,
  `rumbo_grados_rg` DECIMAL(10,5) NOT NULL,
  `rumbo_distancia_rd` DECIMAL(10,5) NOT NULL,
  `gps_interval` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `fepsa_remises`.`pasajeros_fepsa` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `legajo` VARCHAR(45) NOT NULL,
  `nombre_apellido` VARCHAR(45) NOT NULL,
  `id_version` BIGINT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `fepsa_remises`.`pasajeros_viajes` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `id_viaje` BIGINT NOT NULL,
  `id_pasajero` BIGINT NOT NULL,
  `id_posicion_sube` BIGINT NOT NULL,
  `id_posicion_baja` BIGINT NOT NULL,
  `nombre_apellido` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `fepsa_remises`.`versiones_pasajeros` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `detalles` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `fepsa_remises`.`viajes` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `nro_viaje` BIGINT NOT NULL,
  `id_chofer` BIGINT NOT NULL,
  `fecha_inicio` DATETIME NOT NULL,
  `fecha_fin` DATETIME NOT NULL,
  `t_espera` BIGINT NOT NULL,
  `peajes` DECIMAL(10,5) NOT NULL,
  `ticket` DECIMAL(10,5) NOT NULL,
  PRIMARY KEY (`id`));


  ALTER TABLE `viajes`
    ALTER `nro_viaje` DROP DEFAULT,
    ALTER `id_chofer` DROP DEFAULT;
ALTER TABLE `viajes`
    CHANGE COLUMN `nro_viaje` `nro_viaje` BIGINT(20) UNSIGNED NOT NULL AFTER `id`,
    CHANGE COLUMN `id_chofer` `id_chofer` BIGINT(20) UNSIGNED NOT NULL AFTER `nro_viaje`,
    ADD INDEX `nro_viaje_id_chofer` (`nro_viaje`, `id_chofer`);
