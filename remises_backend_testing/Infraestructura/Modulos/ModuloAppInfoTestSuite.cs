﻿using System;
using System.Linq;
using Nancy.Bootstrapper;
using Nancy;
using Nancy.Testing;
using Xunit;
using FakeItEasy;
using AutoMapper;
using NPoco;
using System.Collections.Generic;
using remises_backend.Infraestructura.Mappers;

namespace remises_backend_testing.Infraestructura.Modulos
{
    using remises_backend.Infraestructura.Modulos;
    using remises_backend.AccesoDatos.Tablas;
    using remises_backend.Infraestructura.Repositorios;
    using remises_backend.Infraestructura.Dtos;
    using Newtonsoft.Json;

    public class ModuloAppInfoTestSuite
    {

        [Fact]
        public void deberia_poder_ser_instanciado()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<ParametroLogica>>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var fakeVersionParametros = obtenerRepositorioPasajeros(null);
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorio<ParametroLogica>>(fakeRepo);
                with.Dependency<IRepositorioVersionPasajero>(fakeVersionParametros);
                with.Module<ModuloAppInfo>();
            });

            // when
            new Browser(bootstrapper);
        }

        [Fact]
        public void deberia_fallar_si_no_hay_datos_cargados()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<ParametroLogica>>();           
            var lista = new List<ParametroLogica>();
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);

            var aFakeDto = A.Fake<ParametrosAppGet>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            A.CallTo(() => fakeMapper.Map<ParametroLogica, ParametrosAppGet>(
                A<ParametroLogica>._,
                A<Action<AutoMapper.IMappingOperationOptions<ParametroLogica, ParametrosAppGet>>>._)).Returns(aFakeDto);
            
            var fakeVersionParametros = obtenerRepositorioPasajeros(null);

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorio<ParametroLogica>>(fakeRepo);
                with.Dependency<IRepositorioVersionPasajero>(fakeVersionParametros);
                with.Module<ModuloAppInfo>();
            });

            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Get(Rutas.AppInfoBase, with =>
                {
                    with.HttpRequest();
                });
            };

            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void deberia_devolver_ok_si_tiene_datos_cargados()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<ParametroLogica>>();
            var fakeParametro = A.Fake<ParametroLogica>();
            var lista = new List<ParametroLogica>() { fakeParametro };
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);
            var fakeVersionParametros = obtenerRepositorioPasajeros(null);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorio<ParametroLogica>>(fakeRepo);
                with.Dependency<IRepositorioVersionPasajero>(fakeVersionParametros);
                with.Module<ModuloAppInfo>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Get(Rutas.AppInfoBase, with =>
            {
                with.HttpRequest();
            });

            // then
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }
        
        [Fact]
        public void deberia_llamar_al_mapper_dto()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<ParametroLogica>>();
            var fakeParametro = A.Fake<ParametroLogica>();
                           
            var lista = new List<ParametroLogica>() { fakeParametro };
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);
            
            var fakeVersionParametros = obtenerRepositorioPasajeros(null);
            
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorio<ParametroLogica>>(fakeRepo);
                with.Dependency<IRepositorioVersionPasajero>(fakeVersionParametros);
                with.Module<ModuloAppInfo>();
            });
            var browser = new Browser(bootstrapper);
            
            // when
            var result = browser.Get(Rutas.AppInfoBase, with =>
            {
                with.HttpRequest();
            });

            // then
            A.CallTo(() => fakeMapper.Map<ParametroLogica, ParametrosAppGet>(A<ParametroLogica>._,
                A<Action<IMappingOperationOptions<ParametroLogica, ParametrosAppGet>>>._)).MustHaveHappened();
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public void deberia_devolver_los_datos_esperados_correctamente()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<ParametroLogica>>();
            var fakeParametro = new ParametroLogica()
            {
                OnOffRumbo = true,
                OnOffTiempoVelocidad = false,
                OnOffDistanciaVelocidad = true,                
                Velocidad1 = 10,
                Velocidad2 = 10,
                Velocidad3 = 11,
                Velocidad4 = 13,
                TiempoT1 = 10,
                TiempoT2 = 11,
                TiempoT3 = 1,
                DistanciaX1 = 2,
                DistanciaX2 = 3,
                DistanciaX3 = 4,
                RumboGrados = 5,
                RumboDistancia = 7,
                GpsInterval = 9
            };
                           
            var lista = new List<ParametroLogica>() { fakeParametro };
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);

            var versionPasajero = new VersionPasajero()
            {
                Id = 1,
                detalle = "hola que tal"
            };
            var fakeVersionParametros = obtenerRepositorioPasajeros(versionPasajero);
                                   
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {                
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioVersionPasajero>(fakeVersionParametros);
                with.Dependency<IRepositorio<ParametroLogica>>(fakeRepo);
                with.Module<ModuloAppInfo>();
            });
            var browser = new Browser(bootstrapper);
            
            // when
            var result = browser.Get(Rutas.AppInfoBase, with =>
            {
                with.HttpRequest();
            });

            // then
            var actual = result.Body.DeserializeJson<ParametrosAppGet>();
            
            Assert.Equal(fakeParametro.OnOffRumbo, actual.controlRumbo);
            Assert.Equal(fakeParametro.OnOffTiempoVelocidad, actual.controlTiempoVelocidad);
            Assert.Equal(fakeParametro.OnOffDistanciaVelocidad, actual.controlDistanciaVelocidad);            
            Assert.Equal(fakeParametro.Velocidad1, actual.velocidad1);
            Assert.Equal(fakeParametro.Velocidad2, actual.velocidad2);
            Assert.Equal(fakeParametro.Velocidad3, actual.velocidad3);
            Assert.Equal(fakeParametro.Velocidad4, actual.Velocidad4);
            Assert.Equal(fakeParametro.TiempoT1, actual.tiempo1);
            Assert.Equal(fakeParametro.TiempoT2, actual.tiempo2);
            Assert.Equal(fakeParametro.TiempoT3, actual.tiempo3);
            Assert.Equal(fakeParametro.DistanciaX1, actual.distancia1);            
            Assert.Equal(fakeParametro.DistanciaX1, actual.distancia1);
            Assert.Equal(fakeParametro.DistanciaX2, actual.distancia2);
            Assert.Equal(fakeParametro.DistanciaX3, actual.distancia3);
            Assert.Equal(fakeParametro.RumboGrados, actual.rumboGrados);
            Assert.Equal(fakeParametro.RumboDistancia, actual.rumboDistancia);
            Assert.Equal(fakeParametro.GpsInterval, actual.gpsInterval);
            Assert.Equal(versionPasajero.Id, actual.versionPasajeros);
        }
        
        private static IRepositorioVersionPasajero obtenerRepositorioPasajeros(VersionPasajero value)
        {
            var aFakeVersionPasajero = A.Fake<IRepositorioVersionPasajero>();
            A.CallTo(() => aFakeVersionPasajero.obtenerUltimaVersion()).Returns(value);
            return aFakeVersionPasajero;
        }
    }    
}
