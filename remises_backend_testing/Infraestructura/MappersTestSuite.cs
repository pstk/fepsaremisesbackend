﻿using AutoMapper;
using Newtonsoft.Json;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Dtos;
using remises_backend.Infraestructura.Mappers;
using Xunit;

namespace remises_backend_testing.Infraestructura
{
    public class Mappers
    {
        [Fact]
        public void la_configuracion_deberia_ser_valida()
        {
            MapperFactory.GetInstance();
            Mapper.AssertConfigurationIsValid();
        }
        
        [Fact]
        public void parametros_app_post_deberia_mapearse_sin_fallar()
        {
            var cuerpo = obtenerPostValido();
            MapperFactory.GetInstance().Map<ParametrosAppPost, ParametroLogica>(cuerpo);
        }
        
        [Fact]
        public void viaje_post_deberia_mapearse_sin_fallar()
        {
            var cuerpo = obtenerPostPasajerosValido();
            
            JsonConvert.DeserializeObject<ViajePost>(cuerpo);
        }        

        private ParametrosAppPost obtenerPostValido()
        {
            var json = @"
            {
                'controlRumbo': true,
                'controlTiempoVelocidad': true,
                'controlDistanciaVelocidad': true,
                'velocidad1': 1.4,
                'velocidad2': 30.5,
                'velocidad3': 1.4,
                'velocidad4': 30.5,
                'tiempo1': 600,
                'tiempo2': 120,
                'tiempo3': 30,
                'distancia1': 100,
                'distancia2': 3000,
                'distancia3': 3000,
                'rumboGrados': 30,
                'rumboDistancia': 70,
                'gpsInterval': 3
            }";
            
            return JsonConvert.DeserializeObject<ParametrosAppPost>(json);
        }

        private string obtenerPostPasajerosValido()
        {
            var json = @"{
                'choferId': 1,
                'fechaFin': '2017-12-20T14:04:28.985Z',
                'fechaInicio': '2017-12-20T14:03:58.217Z',
                'numeroViaje': 4444,
                'pasajeros': [
                {
                    'pasajeroId': null,
                    'pasajeroNombre': 'Gonzalo',
                    'posBaja': 1513810974000,
                    'posSube': 1513810974000
                }
                ],
                'peajes': 20.5,
                'ticket': 0.0, 
                'posiciones': [
                {
                    'fecha': '1969-12-31T21:00:00.000Z',
                    'id': 0,
                    'latitud': 0,
                    'longitud': 0,
                    'rumbo': 0,
                    'accuracy': 0,
                    'velocidad': 0,
                    'versionLogica': 3
                },
                {
                    'fecha': '2017-12-20T20:02:54.000Z',
                    'id': 1513810974000,
                    'latitud': -62.2678,
                    'longitud': -38.7192983,
                    'rumbo': 20,
                    'accuracy': 0,
                    'velocidad': 0,
                    'versionLogica': 3
                }
                ],
                'tiempoEspera': 4444
            }";
            return json;
        }
    }
}