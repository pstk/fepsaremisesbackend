﻿using System;
using System.Data;
using System.Data.Common;
using System.IO;
using NPoco;
using NPoco.FluentMappings;

namespace remises_backend.AccesoDatos
{
    public static class MyFactory
    {

        private static Database getDatabase()
        {
            var dbConnectionString = "Server=db_fepsa;Uid=fepsa_remises;Pwd=remises01;Database=fepsa_remises;Port=3306";
            var db = new Database(dbConnectionString, DatabaseType.MySQL);
            return db;
        }

        public static DatabaseFactory DbFactory { get; set; }

        public static void Setup()
        {
            DbFactory = DatabaseFactory.Config(x =>
            {
                x.UsingDatabase(getDatabase);
            });
        }
    }    
}
