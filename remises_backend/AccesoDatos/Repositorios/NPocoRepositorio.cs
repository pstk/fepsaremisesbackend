﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;
using remises_backend.AccesoDatos.Tablas;

namespace remises_backend.AccesoDatos.Repositorios
{
    using remises_backend.Infraestructura.Repositorios;
    using remises_backend.Infraestructura;

    public class NPocoRepositorio<T> : IRepositorio<T> where T : Entidad
    {
        protected readonly IDatabase db;

        public NPocoRepositorio(IDatabase db) {
            this.db = db;
        }

        public List<T> ObtenerTodos()
        {
            return this.db.Query<T>()
                .OrderByDescending(x => x.Id)
                .ToList();
        }

        public int Count()
        {
            return this.db.Query<T>().Count();
        }

        public void Guardar(T entidad)
        {
            this.db.Save(entidad);
        }
    }

    public class NPocoRepositorioPosiciones : NPocoRepositorio<Posicion>, IRepositorioPosiciones
    {
        public NPocoRepositorioPosiciones(IDatabase db) : base(db) { }

        public List<Posicion> guardarPosiciones(List<Posicion> posiciones)
        {
            if (posiciones.Count == 0)
                return new List<Posicion>();
          
            this.db.InsertBulk(posiciones);
            return this.db.Query<Posicion>()
                .Where(x => x.IdViaje == posiciones[0].IdViaje).ToList();
        }
    }

    public class NPocoRepositorioPasajeros : NPocoRepositorio<PasajeroViaje>, IRepositorioPasajeros
    {
        public NPocoRepositorioPasajeros(IDatabase db) : base(db) { }

        public void guardarPasajeros(List<PasajeroViaje> pasajeros)
        {
            this.db.InsertBulk(pasajeros);
        }
    }
    
    public class NPocoRepositorioVersionPasajero : NPocoRepositorio<VersionPasajero>, IRepositorioVersionPasajero
    {
        public NPocoRepositorioVersionPasajero(IDatabase db) : base(db)
        {
        }

        public VersionPasajero obtenerUltimaVersion()
        {
            var a = this.db.Query<VersionPasajero>()
                .OrderBy(x => x.Id)
                .Limit(1)
                .ToList();
            if (a.Count == 0)
            {
                return null;
            }
            else
            {
                return a[0];
            }
        }
    }

    public class NPocoRepositorioViajes : NPocoRepositorio<Viaje>, IRepositorioViajes
    {
        public NPocoRepositorioViajes(IDatabase db) : base(db)
        {
        }

        public long guardarViaje(Viaje viaje)
        {
            this.db.Insert(viaje);
            return viaje.Id;
        }
    }

    public class NPocoRepositorioChoferes : NPocoRepositorio<Chofer>, IRepositorioChoferes
    {
        public NPocoRepositorioChoferes(IDatabase db) : base(db) { }

        public Chofer buscarChofer(string dni, string clavePrecompartida)
        {
            var sql = Sql.Builder
                .Select("c.*")
                .From("choferes c")
                .InnerJoin("remiseras r").On("c.id_remisera = r.id")
                .Where("c.dni = @0 and  r.codigo_activacion = @1", dni, clavePrecompartida);

            var resultado = db.Fetch<Chofer>(sql)
                .FirstOrDefault();
            return resultado;
        }

        public void guardarChofer(Chofer nuevoChofer)
        {
            this.db.Insert<Chofer>(nuevoChofer);
        }
    }

    public class NPocoRepositorioRemiseras : NPocoRepositorio<Remisera>, IRepositorioRemiseras
    {
        public NPocoRepositorioRemiseras(IDatabase db) : base(db)
        {
        }

        public Remisera buscarRemisera(string clavePrecompartida)
        {
            var sql = Sql.Builder
                .Select("r.*")
                .From("remiseras r")
                .Where("r.codigo_activacion = @0", clavePrecompartida);
            
            return this.db.FirstOrDefault<Remisera>(sql);
        }
    }
}
