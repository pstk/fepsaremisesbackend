﻿using System;
using NPoco;
using remises_backend.Infraestructura;

namespace remises_backend.AccesoDatos.Tablas
{
    [TableName("posiciones")]
    [PrimaryKey("id")]
    public class Posicion : Entidad
    {
        [Column("id")]
        public override long Id { get; set; }
        
        [Column("id_viaje")]
        public long IdViaje { get; set; }

        [Column("id_interno")]
        public long IdViajeInterno { get; set; }
        
        [Column("fecha")]
        public DateTime fecha { get; set; }

        [Column("latitud")]
        public decimal latitud { get; set; }

        [Column("longitud")]
        public decimal longitud { get; set; }

        [Column("rumbo")]
        public decimal Rumbo { get; set; }

        [Column("accuracy")]
        public decimal Accuracy { get; set; }

        [Column("velocidad")]
        public decimal Velocidad { get; set; }

        [Column("fecha_insercion")]
        public DateTime FechaInsercion { get; set; }

        [Column("id_version_logica")]
        public long IdVersionLogica { get; set; }

        public Posicion() { }
    }
}
