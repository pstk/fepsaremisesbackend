﻿using System;
using NPoco;
using remises_backend.Infraestructura;

namespace remises_backend.AccesoDatos.Tablas
{
    [TableName("viajes")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class Viaje : Entidad
    {
        [Column("id")]
        public override long Id { get; set; }

        [Column("id_chofer")]
        public long IdChofer { get; set; }

        [Column("fecha_inicio")]
        public DateTime FechaInicio { get; set; }
        
        [Column("nro_viaje")]
        public long NumeroViaje { get; set; }

        [Column("fecha_fin")]
        public DateTime FechaFin { get; set; }

        [Column("t_espera")]
        public long TiempoEspera { get; set; }

        [Column("peajes")]
        public long Peajes { get; set; }
        
        [Column("ticket")]
        public decimal Ticket { get; set; }

        public Viaje() { }
    }
}
