﻿using System;
using NPoco;
using remises_backend.Infraestructura;

namespace remises_backend.AccesoDatos.Tablas
{
    [TableName("versiones_pasajeros")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class VersionPasajero : Entidad
    {
        [Column("id")]
        public override long Id { get; set; }
        
        [Column("detalles")]
        public string detalle { get; set; }
        
       
        public VersionPasajero() { }
    }

}