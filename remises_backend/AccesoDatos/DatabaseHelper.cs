﻿using System;
using NPoco;

namespace remises_backend.AccesoDatos
{
    public class DatabaseHelper
    {
        public DatabaseHelper()
        {
            // Or the fluent configuration
            var db = DatabaseConfiguration
                     .Build()
                     .UsingConnectionString("Data Source=./TestDb.sqlite;Version=3;")
                     .UsingDefaultMapper<ConventionMapper>(m =>
                     {
                         m.FromDbConverter = (targetProperty, sourceType) =>
                         {
                             if (targetProperty != null && sourceType == typeof(long))
                             {
                                 var type = !targetProperty.PropertyType.IsNullableType()
                                     ? targetProperty.PropertyType
                                     : targetProperty.PropertyType.GetGenericArguments().First();

                                 switch (Type.GetTypeCode(type))
                                 {
                                     case TypeCode.DateTime:
                                         return o => new DateTime((long)o, DateTimeKind.Utc);
                                     default:
                                         return o => Convert.ChangeType(o, Type.GetTypeCode(type));
                                 }
                             }

                             return null;
                         };
                         m.ToDbConverter = sourceProperty =>
                         {
                             var type = !sourceProperty.PropertyType.IsNullableType()
                                 ? sourceProperty.PropertyType
                                 : sourceProperty.PropertyType.GetGenericArguments().First();

                             switch (Type.GetTypeCode(type))
                             {
                                 case TypeCode.DateTime:
                                     return o => ((DateTime)o).Ticks;
                             }

                             return null;
                         };
                     }).Create();
        }
    }
}
