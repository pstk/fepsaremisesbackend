﻿using System;
using Nancy;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace remises_backend
{
    using Modulos;

    public class HelloModule : NancyModule
    {
        public HelloModule()
        {
            Get["/app-info"] = parameters => HandlerAppInfo.handle();

            Post["/activar-chofer"] = parameters => HandlerActivarChofer.handle();

            Get["/posibles-pasajeros"] = parameters => HandlePosiblesPasajeros.handle();

            Post["/agregar-viaje"] = parameters => HandlerAgregarViaje.handle();
        }
    }
}

