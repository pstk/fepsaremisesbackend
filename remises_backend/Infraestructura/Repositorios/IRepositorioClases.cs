﻿using remises_backend.AccesoDatos.Tablas;

namespace remises_backend.Infraestructura.Repositorios
{
    public interface IRepositorioViajes : IRepositorio<Viaje>
    {
        long guardarViaje(Viaje viaje);
    }
}