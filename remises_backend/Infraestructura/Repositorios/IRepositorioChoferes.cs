﻿using System;
using remises_backend.AccesoDatos.Tablas;

namespace remises_backend.Infraestructura.Repositorios
{
    public interface IRepositorioChoferes : IRepositorio<Chofer>
    {
        Chofer buscarChofer(String dni, String clavePrecompartida);
        void guardarChofer(Chofer nuevoChofer);
    }

    public interface IRepositorioRemiseras : IRepositorio<Remisera>
    {
        Remisera buscarRemisera(String clavePrecompartida);
    }
}