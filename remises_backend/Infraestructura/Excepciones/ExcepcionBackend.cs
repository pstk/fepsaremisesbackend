﻿using System;
using Nancy;

namespace remises_backend.Infraestructura.Excepciones
{
    public class ExcepcionBackend : Exception
    {
        public readonly HttpStatusCode StatusCode;

        public ExcepcionBackend(String message, HttpStatusCode statusCode) : base(message)
        {
            this.StatusCode = statusCode;
        }
    }
}