﻿using System;
namespace remises_backend.Infraestructura.Modulos
{
    public static class Constantes
    {
        public readonly static string Version = "7.0.0";
        
    }
    
    public static class Rutas
    {
        public readonly static string Fullcheck = "/fullcheck";
        public readonly static string Alive = "/alive";
        public readonly static string AppInfoBase = "/app-info";
        public readonly static string PosiblesPasajeros = "/posibles-pasajeros";
        public readonly static string ActivarChofer = "/activar-chofer";
        public readonly static string Viajes = "/viajes";
    }
}
