﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using AutoMapper;
using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;
using Nancy.Routing.Constraints;
using Newtonsoft.Json;
using NPoco;
using NPoco.fastJSON;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Dtos;
using remises_backend.Infraestructura.Excepciones;
using remises_backend.Infraestructura.Modulos;
using remises_backend.Infraestructura.Repositorios;
using IMapper = AutoMapper.IMapper;
using System.Data.SqlClient;

namespace remises_backend.Modulos
{
    public class ModuloViajes : NancyModule
    {
        private static readonly String EmptyJson = "{}";

        public ModuloViajes(IRepositorioViajes repoViajes, 
            IRepositorioPasajeros repoPasajeros,
            IRepositorioPosiciones repoPosiciones,
            IMapper mapper) : base()
        {
            Post[Rutas.Viajes] = _ => agregarViajes(repoViajes, repoPasajeros, repoPosiciones, mapper);
        }

        private string agregarViajes(IRepositorioViajes repoViajes, IRepositorioPasajeros repoPasajeros,
            IRepositorioPosiciones repoPosiciones, IMapper mapper)
        {
            var dto = deserializarCuerpo();

            var viaje = mapper.Map<Viaje>(dto);

            long idViaje;
            try {
                idViaje = repoViajes.guardarViaje(viaje);
            } catch (SqlException) {
                // si el viaje ya existe, mentir y decir que fue insertado OK
                return EmptyJson;
            }

            if (dto.posiciones is null || dto.posiciones.Count <= 0) return EmptyJson;

            var posiciones = (mapper.Map<List<Posicion>>(dto.posiciones))
                .Select(c =>
                {
                    c.IdViaje = idViaje;
                    return c;
                }).ToList();

            var posicionesPersistidas = repoPosiciones.guardarPosiciones(posiciones);

            Dictionary<long, Posicion> dictPosicionesPersistidas = null;
            try
            {
                dictPosicionesPersistidas = posiciones.ToDictionary(x => x.IdViajeInterno, x => x);
            }
            catch (ArgumentException e)
            {
                throw e;
            }
            if (dto.pasajeros is null || dto.pasajeros.Count <= 0) return EmptyJson;

            var pasajeros = dto.pasajeros
                .Select(x =>
                {
                    x.posSube = obtenerIdPosicionSube(x.posSube, dictPosicionesPersistidas);
                    x.posBaja = obtenerIdPosicionBaja(x.posBaja, dictPosicionesPersistidas);
                    return x;
                })
                .Select(x =>
                {
                    var c = mapper.Map<PasajeroViaje>(x);
                    c.IdViaje = idViaje;
                    return c;
                })
                .ToList();

            repoPasajeros.guardarPasajeros(pasajeros);

            return EmptyJson;
        }

        private static long obtenerIdPosicionBaja(long argPosBaja, Dictionary<long, Posicion> dictPosicionesPersistidas)
        {
            return dictPosicionesPersistidas[argPosBaja].Id;
        }

        private static long obtenerIdPosicionSube(long argPosSube, Dictionary<long, Posicion> dictPosicionesPersistidas)
        {
            return dictPosicionesPersistidas[argPosSube].Id;
        }

        private ViajePost deserializarCuerpo()
        {
            ViajePost post = null;
            try
            {
                post = JsonConvert.DeserializeObject<ViajePost>(Request.Body.AsString());
            }
            catch (Exception)
            {
                throw new CuerpoDelPedidoInvalido();
            }
            return post;
        }

        private class CuerpoDelPedidoInvalido : ExcepcionBackend
        {
            public CuerpoDelPedidoInvalido() :
                base("El cuerpo del pedido no es el esperado (ver swagger api)", HttpStatusCode.BadRequest)
            { }
        }
    }
}
