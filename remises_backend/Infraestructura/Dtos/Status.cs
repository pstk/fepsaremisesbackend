﻿namespace remises_backend.Infraestructura.Dtos
{
    class StatusFullGet
    {
        public string version { get; set; }
        public int cantidad_registros_parametros_logica { get; set; }
        public int cantidad_registros_choferes { get; set; }
        public int cantidad_registros_pasajeros_fepsa { get; set; }
        public int cantidad_registros_pasajeros_viajes { get; set; }
        public int cantidad_registros_posiciones { get; set; }
        public int cantidad_registros_remiseras { get; set; }
        public int cantidad_registros_versiones_pasajeros { get; set; }
        public int cantidad_registros_viajes { get; set; }
    }
    
    class StatusAliveGet
    {
        public string version { get; set; }
    }
}