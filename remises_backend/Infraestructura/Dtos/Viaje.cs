﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace remises_backend.Infraestructura.Dtos
{

    public class PasajeroPost
    {
        public long? pasajeroId { get; set; }
        public string pasajeroNombre { get; set; }
        public long posSube { get; set; }
        public long posBaja { get; set; }
    }

    public class PosicionPost
    {
        public long id { get; set; }
        public decimal latitud { get; set; }
        public decimal longitud { get; set; }
        public decimal velocidad { get; set; }
        public decimal rumbo { get; set; }
        public decimal accuracy { get; set; }
        public DateTime fecha { get; set; }
        public long versionLogica { get; set; }
    }

    public class ViajePost
    {
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public long choferId { get; set; }
        public long tiempoEspera { get; set; }
        public decimal peajes { get; set; }
        public decimal ticket { get; set; }
        public long numeroViaje { get; set; }
        public List<PasajeroPost> pasajeros { get; set; }
        public List<PosicionPost> posiciones { get; set; }
    }
}
